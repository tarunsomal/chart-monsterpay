#!/bin/bash
set -e

service=$1

RESPONSE=`curl -s --connect-timeout 5 --max-time 10 --retry 5 --retry-delay 0 --retry-max-time 60 --fail http://${service}/wallets/lisa@gmail.com`

if [ -n "$RESPONSE" ]; then
  NUM_WALLETS=`echo $RESPONSE | python -c "import sys, json; print len(json.load(sys.stdin)['wallets'])"`
  if [ $NUM_WALLETS  -gt 0 ]; then
    echo end to end test succeeds!
    exit 0
  fi
fi

echo end to end test fails!
exit 1